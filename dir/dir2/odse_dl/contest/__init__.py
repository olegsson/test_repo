import requests
import json
import rasterio as rio
import numpy as np
import os
from pathlib import Path
from typing import Union

BASE_URL = 'http://127.0.0.1:5000/'
CWD = Path(os.getcwd())
AUTH_FILE = CWD / 'odse2022_contest_credentials.json'

def get_creds(
    verbose: bool=True,
) -> Union[dict, None]:
    try:
        with open(AUTH_FILE) as src:
            return json.load(src)
    except FileNotFoundError:
        if verbose:
            print(
                'Contestant credentials not found, please register.',
                f'If you have already registered, run this code from a directory containing {AUTH_FILE.name}',
            sep='\n')
        return None

def register(
    name: str,
):
    creds = get_creds(verbose=False)
    if creds is not None:
        print(
            f'You are already registered as {creds["name"]}',
        )
        return

    resp = requests.post(
        BASE_URL+'register',
        data=json.dumps({
            'name': name,
        })
    )
    if resp.status_code == 406:
        print(f'Name {name} is already taken.')
    elif resp.status_code == 200:
        data = resp.json()
        with open(AUTH_FILE, 'w') as dst:
            json.dump(data, dst, indent=4)
        print(
            f'Thanks for participating, {name}!',
            f'Credentials saved to {AUTH_FILE}',
        sep='\n')

def submit(
    data: Union[np.ndarray, Path, str],
):

    creds = get_creds()
    if creds is None:
        return

    if isinstance(data, (Path, str)):
        with rio.open(data) as src:
            data = src.read(1)

    shape = np.array(data.shape)
    if shape.size > 2:
        data = data.reshape(shape[shape>1])

    resp = requests.post(
        BASE_URL+'submit',
        data=json.dumps({
            'name': creds['name'],
            'token': creds['token'],
            'data': data.tolist(),
        })
    )

    if resp.status_code == 429:
        print(
            'Submition rate is limited to once in 5 minutes.',
            'Please try again later.',
        sep='\n')

    else:
        print(resp.text)

def scoreboard():
    resp = requests.get(BASE_URL+'scoreboard')
    print(resp.text)
