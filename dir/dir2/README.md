# ODSE 2022 Deep Learning utility package

## Install

`pip install git+https://gitlab.com/olegsson/odse2022_dl-training#odse_dl`

## Usage

### Data utilities

`In [1]: from odse_dl import data`

The `data` submodule includes helpers for loading the target data and downloading inputs aligned to the target tiles.

`target_files` contains a dict of target files for each tile.

```
In [2]: data.target_files
Out[2]:
{'t1': PosixPath('/home/luka/miniconda3/envs/gis39/lib/python3.9/site-packages/odse_dl-0.1.0-py3.9.egg/odse_dl/dist_data/target_t1.tif'),
 't2': PosixPath('/home/luka/miniconda3/envs/gis39/lib/python3.9/site-packages/odse_dl-0.1.0-py3.9.egg/odse_dl/dist_data/target_t2.tif'),
 't3': PosixPath('/home/luka/miniconda3/envs/gis39/lib/python3.9/site-packages/odse_dl-0.1.0-py3.9.egg/odse_dl/dist_data/target_t3.tif'),
 't4': PosixPath('/home/luka/miniconda3/envs/gis39/lib/python3.9/site-packages/odse_dl-0.1.0-py3.9.egg/odse_dl/dist_data/target_t4.tif'),
 't5': PosixPath('/home/luka/miniconda3/envs/gis39/lib/python3.9/site-packages/odse_dl-0.1.0-py3.9.egg/odse_dl/dist_data/target_t5.tif'),
 't7': PosixPath('/home/luka/miniconda3/envs/gis39/lib/python3.9/site-packages/odse_dl-0.1.0-py3.9.egg/odse_dl/dist_data/target_t7.tif'),
 't8': PosixPath('/home/luka/miniconda3/envs/gis39/lib/python3.9/site-packages/odse_dl-0.1.0-py3.9.egg/odse_dl/dist_data/target_t8.tif'),
 't9': PosixPath('/home/luka/miniconda3/envs/gis39/lib/python3.9/site-packages/odse_dl-0.1.0-py3.9.egg/odse_dl/dist_data/target_t9.tif')}
```

`get_tile_geometries()` returns a `geopandas.GeoDataFrame` of tile names and bounding boxes.

```
In [3]: data.get_tile_geometries()
Out[3]:
  name                                           geometry
0   t1  POLYGON ((4634730.000 2993230.000, 4634730.000...
1   t2  POLYGON ((4634730.000 3000730.000, 4634730.000...
2   t3  POLYGON ((4634730.000 3008230.000, 4634730.000...
3   t4  POLYGON ((4642230.000 2993230.000, 4642230.000...
4   t5  POLYGON ((4642230.000 3000730.000, 4642230.000...
5   t6  POLYGON ((4642230.000 3008230.000, 4642230.000...
6   t7  POLYGON ((4649730.000 2993230.000, 4649730.000...
7   t8  POLYGON ((4649730.000 3000730.000, 4649730.000...
8   t9  POLYGON ((4649730.000 3008230.000, 4649730.000...
```

`input_data_to_tiles()` arranges input rasters into tiles and returns a dict containing a list of input files for each tile.

```
In [4]: files = data.input_data_to_tiles(
   ...:     ['input_raster.tif'], # iterable of input rasters
   ...:     out_dir='./',         # defaults to working directory
   ...:     n_threads=8,          # defaults to logical cpu count
   ...: )
[22:16:05] clipped datasets to tile 1 of 9
[22:16:05] clipped datasets to tile 2 of 9
[22:16:05] clipped datasets to tile 3 of 9
[22:16:05] clipped datasets to tile 4 of 9
[22:16:05] clipped datasets to tile 5 of 9
[22:16:05] clipped datasets to tile 6 of 9
[22:16:05] clipped datasets to tile 7 of 9
[22:16:05] clipped datasets to tile 8 of 9
[22:16:05] clipped datasets to tile 9 of 9

In [5]: files
Out[5]:
{'t1': [PosixPath('input_raster_t1.tif')],
 't2': [PosixPath('input_raster_t2.tif')],
 't3': [PosixPath('input_raster_t3.tif')],
 't4': [PosixPath('input_raster_t4.tif')],
 't5': [PosixPath('input_raster_t5.tif')],
 't6': [PosixPath('input_raster_t6.tif')],
 't7': [PosixPath('input_raster_t7.tif')],
 't8': [PosixPath('input_raster_t8.tif')],
 't9': [PosixPath('input_raster_t9.tif')]}
```

`get_sentinel_tiles()` is a convenience function that calls `input_data_to_tiles` on `data.sentinel_urls` to download the Geo-harmonizer Sentinel 2 seasonal mosaics for 2018 (with identical keyword arguments).

```
In [6]: files = data.get_sentinel_tiles()
[22:21:57] downloaded datasets for tile 1 of 9
[22:22:22] downloaded datasets for tile 2 of 9
...

In [7]: files
Out[8]:
{'t1': [PosixPath('input_data/lcv_blue_sentinel.s2l2a_p25_30m_0..0cm_2017.12.02..2018.03.20_eumap_epsg3035_v1.0_t1.tif'),
  PosixPath('input_data/lcv_blue_sentinel.s2l2a_p25_30m_0..0cm_2018.03.21..2018.06.24_eumap_epsg3035_v1.0_t1.tif'),
  PosixPath('input_data/lcv_blue_sentinel.s2l2a_p25_30m_0..0cm_2018.06.25..2018.09.12_eumap_epsg3035_v1.0_t1.tif'),
  PosixPath('input_data/lcv_blue_sentinel.s2l2a_p25_30m_0..0cm_2018.09.13..2018.12.01_eumap_epsg3035_v1.0_t1.tif'),
...
```

### DL contest client

The `contest` module provides means to register and submit results to the contest.

Calling `register()` with a name you wish to use for the contest takes care of registration and saves your credentials to the current working directory.

```
In [1]: from odse_dl import contest

In [2]: contest.register('Contestant')
Thanks for participating, Contestant!
Credentials saved to /home/contestant/odse2022_contest_credentials.json
```

You can `submit()` your results by passing either a path to a GeoTiff of a NumPy array...

```
In [3]: contest.submit('results.tif')
              precision    recall  f1-score   support

           0       0.01      0.01      0.01      2604
           1       0.26      0.37      0.30     17971
           2       0.13      0.19      0.16      9457
           3       0.09      0.16      0.12      7463
           4       0.03      0.08      0.05      2349
           5       0.00      0.00      0.00        90
           6       0.03      0.00      0.01     16453
           7       0.00      0.00      0.00      5362
           8       0.00      0.00      0.00       751

    accuracy                           0.16     62500
   macro avg       0.06      0.09      0.07     62500
weighted avg       0.11      0.16      0.13     62500
```

...and view the current `scoreboard()`.

```
In [4]: contest.scoreboard()
  #  name           score
---  ----------  --------
  1  Contestant  0.128763
```

The contest client can also be used from your preferred shell, e.g.:

```
python -m odse_dl.contest register Contestant
python -m odse_dl.contest submit results.tif
python -m odse_dl.contest scoreboard
```
